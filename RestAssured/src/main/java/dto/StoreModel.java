package dto;

public class StoreModel{
	private Integer petId;
	private Integer quantity;
	private Integer id;
	private String shipDate;
	private Boolean complete;
	private String status;
}
