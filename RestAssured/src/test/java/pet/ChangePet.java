import base.BaseTest;
import dto.PetModel;
import dto.response.ErrorResponse;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.response.ResponseBodyExtractionOptions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.qameta.allure.Allure.step;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static utils.ExpectedObjectBuilder.getUnknownErrorResponse;
import static utils.TestDataHelper.*;
import static utils.TestDataHelper.STATUS_CODE_ERROR_500;
import static utils.TestObjectBuilder.*;

/**
 * Тест сьют метода PUT /pet
 */
@Epic("Pet контроллер")
@Feature("Change the pet in the shop")
public class ChangePet extends BaseTest {

    @Test
    @DisplayName("Change the pet in the shop. Positive case")
    @Story("Изменение добавленного питомца, позитивный сценарий")
    public void ChangeNewPetStorePositive(){
        step("Создание тела запроса",()->{
           /* responseWrapper.extract().jsonPath().get();*/
            request = getAddNewPetModel(VALID_PET_ID);
        });
        step("Выполнение запроса POST /pet для создания питомца", () -> {
            responseWrapper = steps.createNewPetToStore(request);
           response = responseWrapper.as(PetModel.class);
            System.out.println(response);
        });
        step("Выполнение запроса PUT /pet ", () -> {
            request = getChangePetModel(response, "philip");
            responseWrapper = steps.changePetToStore(request);
        });
        step("Проверка результата", () -> {
            int statusCode = responseWrapper.getStatusCode();
            PetModel response = responseWrapper.as(PetModel.class);

            assertSoftly(
                    softAssertions -> {
                        softAssertions
                                .assertThat(statusCode)
                                .withFailMessage("Status code doesn't match")
                                .isEqualTo(STATUS_CODE_OK);
                        softAssertions
                                .assertThat(response)
                                .withFailMessage("Response body doesn't match")
                                .isEqualTo(request);
                    }
            );
        });
    }

    @Test
    @DisplayName("Add new pet to store. Negative case with not valid ID")
    @Story("Добавление нового питомца с не валидным ID, негативный сценарий")
    public void testAddNewPetToStoreNegative() {
        step("Создание тела запроса",()->{
            /* responseWrapper.extract().jsonPath().get();*/
            request = getAddNewPetModel(VALID_PET_ID);
        });
        step("Выполнение запроса POST /pet для создания питомца", () -> {
            responseWrapper = steps.createNewPetToStore(request);
            response = responseWrapper.as(PetModel.class);
            System.out.println(response);
        });
        step("Выполнение запроса PUT /pet ", () -> {


            request = getChangePetModelNotValid(response,NOT_VALID_PET_ID );
            responseWrapper = steps.changePetToStore(request);
        });
        step("Проверка результата", () -> {
            int statusCode = responseWrapper.getStatusCode();
            ErrorResponse error = responseWrapper.as(ErrorResponse.class);

            System.out.println("statusCode="+ statusCode);
            System.out.println("error="+ error);
            assertSoftly(
                    softAssertions -> {
                        softAssertions
                                .assertThat(statusCode)
                                .withFailMessage("Status code doesn't match")
                                .isEqualTo(STATUS_CODE_ERROR_500);
                        softAssertions
                                .assertThat(error)
                                .withFailMessage("Error body doesn't match")
                                .isEqualTo(getUnknownErrorResponse());
                    }
            );
        });
    }
}

