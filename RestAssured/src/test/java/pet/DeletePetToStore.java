import base.BaseTest;
import dto.PetModel;
import dto.response.ErrorResponse;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.qameta.allure.Allure.step;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static utils.ExpectedObjectBuilder.getUnknownErrorResponse;
import static utils.TestDataHelper.*;
import static utils.TestDataHelper.STATUS_CODE_ERROR_500;
import static utils.TestObjectBuilder.getAddNewPetModel;
import static utils.TestObjectBuilder.getChangePetModelNotValid;

/**
 * Тест сьют метода DELETE/pet
 */
@Epic("Pet контроллер")
@Feature("Delete pet to store")
public class DeletePetToStore extends BaseTest {

    @Test
    @DisplayName("Add new pet to store. Positive case")
    @Story("Добавление нового питомца, позитивный сценарий")
    public void DeletePetToStorePositive(){

        step("Создание тела запроса",()->{
            request = getAddNewPetModel(VALID_PET_ID);
        });
        step("Выполнение запроса POST /pet для создания питомца", () -> {
            responseWrapper = steps.createNewPetToStore(request);
            response = responseWrapper.as(PetModel.class);
            System.out.println(response);
        });
        step("Выполнение запроса delete /pet ", () -> {
            responseWrapper = steps.deletePetToStore(Integer.valueOf(response.getId()));
        });

        step("Проверка результата", () -> {
            int statusCode = responseWrapper.getStatusCode();
            ErrorResponse error = responseWrapper.as(ErrorResponse.class);

            System.out.println("statusCode="+ statusCode);
            System.out.println("error="+ error);

            assertSoftly(
                    softAssertions -> {
                        softAssertions
                                .assertThat(statusCode)
                                .withFailMessage("Status code doesn't match")
                                .isEqualTo(STATUS_CODE_OK);
                        softAssertions
                                .assertThat(error.getMessage())
                                .withFailMessage("Response body doesn't match")
                                .isEqualTo(response.getId());
                    }
            );
        });
    }

    @Test
    @DisplayName("Delete pet to store. Negative case with not valid ID")
    @Story("Удаление питомца с не валидным ID, негативный сценарий")
    public void DeletePetToStoreNegative() {
        step("Создание тела запроса с не валидным ID", () -> {
            request = getAddNewPetModel(VALID_PET_ID);
        });
        step("Выполнение запроса POST /pet", () -> {
            responseWrapper = steps.createNewPetToStore(request);
            response = responseWrapper.as(PetModel.class);
        });
        step("Выполнение запроса delete /pet ", () -> {
            responseWrapper = steps.deletePetToStore(Integer.valueOf(response.getId()));
        });
        step("Выполнение запроса delete /pet для удаления удаленного животного ", () -> {
            responseWrapper = steps.deletePetToStore(Integer.valueOf(response.getId()));
        });

        step("Проверка результата", () -> {
            int statusCode = responseWrapper.getStatusCode();

            assertSoftly(
                    softAssertions -> {
                        softAssertions
                                .assertThat(statusCode)
                                .withFailMessage("Status code doesn't match")
                                .isEqualTo(STATUS_CODE_ERROR_404);
                    }
            );
        });
    }
}

