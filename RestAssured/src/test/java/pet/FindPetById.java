import base.BaseTest;
import dto.PetModel;
import dto.response.ErrorResponse;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import utils.ResponseWrapper;

import java.util.List;

import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static utils.TestDataHelper.*;
import static utils.TestDataHelper.STATUS_CODE_OK;

/**
 * Тест сьют метода GET /pet/petId
 */
@Epic("Pet контроллер")
@Feature("Find pets by Id")
public class FindPetById extends BaseTest {

    @Test
    @DisplayName("Find pets by status. Positive case")
    @Story("Поиск питомцев по Id, позитивный сценарий")
    public void testFindPetsByIdPositive(){
        ResponseWrapper responseWrapper = steps.getPetById(Integer.valueOf(VALID_PET_ID));

        int statusCode = responseWrapper.getStatusCode();
        PetModel response = responseWrapper.as(PetModel.class);
        System.out.println("response " );
        System.out.println(response.getId());
        assertSoftly(
                softAssertions -> {
                    softAssertions
                            .assertThat(statusCode)
                            .withFailMessage("Status code doesn't match")
                            .isEqualTo(STATUS_CODE_OK);
                    softAssertions
                            .assertThat(response.getName())
                            .withFailMessage("Response body is empty")
                            .isNotEmpty();
                }
        );
    }

    @Test
    @DisplayName("Find pets by Id. Negative case, if status is not existing")
    @Story("Поиск питомцев с несуществующим Id, негативный сценарий")
    public void testFindPetsByIdIfIdNullNegative(){
        ResponseWrapper responseWrapper = steps.getPetById(Integer.valueOf(NOT_VALID_PET_ID));

        int statusCode = responseWrapper.getStatusCode();
        ErrorResponse response = responseWrapper.as(ErrorResponse.class);

        assertSoftly(
                softAssertions -> {
                    softAssertions
                            .assertThat(statusCode)
                            .withFailMessage("Status code doesn't match")
                            .isEqualTo(STATUS_CODE_ERROR_404);
                    softAssertions
                            .assertThat(response.getType())
                            .withFailMessage("Response body is not empty")
                            .isEqualTo("unknown");
                }
        );
    }
}