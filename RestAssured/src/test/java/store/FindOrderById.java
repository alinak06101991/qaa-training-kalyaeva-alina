package store;

import base.BaseTest;
import dto.PetModel;
import dto.StoreModel;
import dto.response.ErrorResponse;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import utils.ResponseWrapper;

import static io.qameta.allure.Allure.step;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static utils.TestDataHelper.*;
import static utils.TestDataHelper.STATUS_CODE_ERROR_404;
import static utils.TestDataHelper.STATUS_CODE_OK;
import static utils.TestObjectBuilder.getAddNewOrderModel;
import static utils.TestObjectBuilder.getAddNewPetModel;

/**
 * Тест сьют метода GET store/order/orderId
 */
@Epic("Store контроллер")
@Feature("Find store by Id")
public class FindStoreById extends BaseTest {

    @Test
    @DisplayName("Find store by Id. Positive case")
    @Story("Поиск заказа по Id, позитивный сценарий")
    public void testFindOrderByIdPositive(){
        step("Создание тела запроса для создания питомца с валидным ID",()->{
            request = getAddNewPetModel(VALID_PET_ID);
        });
        step("Выполнение запроса POST /pet", () -> {
            responseWrapper = steps.createNewPetToStore(request);
        });
        step("Создание тела запроса для создания заказа с валидным ID",()->{
            PetModel response = responseWrapper.as(PetModel.class);
            requestStore = getAddNewOrderModel(VALID_ORDER_ID, response.getId());
        });
        step("Выполнение запроса POST /store/order", () -> {
            responseWrapper = steps.createNewOrderToStore(requestStore);
        });
        StoreModel resp = responseWrapper.as(StoreModel.class);
        ResponseWrapper responseWrapper = steps.getOrderById(resp.getId());

        int statusCode = responseWrapper.getStatusCode();
        StoreModel response = responseWrapper.as(StoreModel.class);
        assertSoftly(
                softAssertions -> {
                    softAssertions
                            .assertThat(statusCode)
                            .withFailMessage("Status code doesn't match")
                            .isEqualTo(STATUS_CODE_OK);
                    softAssertions
                            .assertThat(response.getId())
                            .withFailMessage("Id is not empty")
                            .isNotEmpty();
                }
        );
    }

    @Test
    @DisplayName("Find order by Id. Negative case, if status is not existing")
    @Story("Поиск заказа с несуществующим Id, негативный сценарий")
    public void testFindOrderByIdIfIdNullNegative(){
        ResponseWrapper responseWrapper = steps.getOrderById(NOT_VALID_ORDER_ID);

        int statusCode = responseWrapper.getStatusCode();
        ErrorResponse response = responseWrapper.as(ErrorResponse.class);

        assertSoftly(
                softAssertions -> {
                    softAssertions
                            .assertThat(statusCode)
                            .withFailMessage("Status code doesn't match")
                            .isEqualTo(STATUS_CODE_ERROR_404);
                    softAssertions
                            .assertThat(response.getType())
                            .withFailMessage("Type is not empty")
                            .isEqualTo("unknown");
                }
        );
    }
}
