import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
/**
 * Класс для проверки строки на соответствие значения
 */
public class single_test {

    /**
     * Тест для метода getText
     */
    @Test(description = "Позитивная проверка на соответствие значения")
    @Severity(SeverityLevel.CRITICAL)
    @Description("Проверяет, что метод getTex выводит Hello World!")
    public void testGetText(){
        Writter writter = new Writter();
        assertEquals(writter.getTex(), "Hello World!");
    }

}
