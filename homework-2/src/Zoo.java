import units.*;
import animals.*;
import food.*;

public class Zoo {

    public static void main(String[] args) {
        Duck duck = new Duck("Дональд", 5, "кря-кря");
        Fish fish = new Fish("Немо", 1);
        Cheetos cheetos = new Cheetos("Лео", 3, "ррррррр");
        Elephant elephant = new Elephant("Питер", 8, "ууууу");
        Lion lion = new Lion("Симба", 6, "РРРРР");
        Rhino rhino = new Rhino("Клаус", 2);
        Worker worker = new Worker("Вася", 40);
        Grass grass = new Grass("трава", 1);
        Meat meat = new Meat("свинина", 2);
        System.out.println(duck.getSatiety());
        worker.feed(grass, duck);
        worker.getVoice(duck);
        System.out.println(lion.getSatiety());
        worker.feed(meat, lion);
        System.out.println(lion.getSatiety());
        worker.getVoice(lion);
        System.out.println(fish.getSatiety());
        worker.feed(grass, fish);
        System.out.println(fish.getSatiety());
        worker.feed(meat, elephant);

        //создать массив - пруд с плавающими животными
        Swim[] pool = {duck, fish};
        for (int i = 0; i < pool.length; i++) {
            pool[i].swim();
        }
    }
}