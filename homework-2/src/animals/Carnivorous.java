package animals;

import food.Food;
import food.Meat;

public class Carnivorous extends Animals {

    public Carnivorous(String name, int satiety, String voice) {
        super(name, satiety, voice);
    }

    public Carnivorous(String name, int satiety) {
        super(name, satiety);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Meat) {
            satiety += food.getSatiety();
            System.out.println("Хищное животное поело мясо");
        } else {
            System.out.println("Хищник не ест траву");
        }
    }
}
