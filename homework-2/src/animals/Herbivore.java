package animals;

import food.Food;
import food.Grass;

public class Herbivore extends Animals {

    public Herbivore(String name, int satiety, String voice) {
        super(name, satiety, voice);
    }

    public Herbivore(String name, int satiety) {
        super(name, satiety);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Grass) {
            satiety += food.getSatiety();
            System.out.println("Травоядное животное поело траву");
        } else {
            System.out.println("Травоядное не ест мясо");
        }
    }
}