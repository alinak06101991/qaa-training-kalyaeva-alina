package animals;

import food.Food;

public abstract class Animals {

    public String getName() {
        return name;
    }

    public int getSatiety() {
        return satiety;
    }

    public String getVoice() {
        return voice;
    }

    protected String name;
    protected int satiety;
    protected String voice;

    //Создать конструктор для всех животных, чтобы задать сытость
    public Animals(String name, int satiety, String voice) {
        this.satiety = satiety;
        this.name = name;
        this.voice = voice;
    }

    public Animals(String name, int satiety) {
        this.satiety = satiety;
        this.name = name;
    }

    public abstract void eat(Food food);

    public String voice () {
        return voice;
    }
}



