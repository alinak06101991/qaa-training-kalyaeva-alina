package units;

import animals.Carnivorous;
import animals.Run;
import animals.Voice;
import food.Food;
import food.Meat;

public class Cheetos extends Carnivorous implements Run, Voice {

    public Cheetos(String name, int satiety, String voice) {
        super(name, satiety, voice);
    }

    @Override
    public void run() {
        System.out.println("Гепард бежит");
    }

    @Override
    public String voice() {
        return voice;
    }

}
