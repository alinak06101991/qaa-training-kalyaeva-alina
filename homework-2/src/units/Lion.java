package units;

import animals.Carnivorous;
import animals.Run;
import animals.Voice;
import food.Food;
import food.Meat;

public class Lion extends Carnivorous implements Run, Voice {

    public Lion(String name, int satiety, String voice) {
        super(name, satiety, voice);
    }
    @Override
    public void run() {
        System.out.println("Lion бежит");
    }

    @Override
    public String voice() {
        return voice;
    }
    }
