package units;

import animals.Animals;
import animals.Voice;
import food.Food;

public class Worker {

    private String nameWorker;
    private int ageWorker;

    public Worker(String nameWorker, int ageWorker) {
        this.ageWorker = ageWorker;
        this.nameWorker = nameWorker;
    }

    public void feed(Food food, Animals animals) {
        animals.eat(food);
    }

    public void getVoice(Voice voice) {
        System.out.println(voice.voice());
    }
}
