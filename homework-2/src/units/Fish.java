package units;

import animals.Carnivorous;
import animals.Swim;
import food.Food;
import food.Meat;

public class Fish extends Carnivorous implements Swim {

    public Fish(String name, int satiety) {
        super(name, satiety);
    }

    @Override
    public void swim() {
        System.out.println("Рыба плывет");
    }
}

