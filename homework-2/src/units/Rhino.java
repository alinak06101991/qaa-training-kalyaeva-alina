package units;

import animals.Herbivore;
import animals.Run;
import animals.Voice;
import food.Food;
import food.Grass;

public class Rhino extends Herbivore implements Run, Voice {

    public Rhino(String name, int satiety) {
        super(name, satiety);
    }
    @Override
    public void run() {
        System.out.println("Носорог бежит");
    }

    @Override
    public String voice() {
        return voice;
    }
}
