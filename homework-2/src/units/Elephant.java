package units;

import animals.Herbivore;
import animals.Run;
import animals.Voice;
import food.Food;
import food.Grass;

public class Elephant extends Herbivore implements Run, Voice {

    public Elephant(String name, int satiety, String voice) {
        super(name, satiety, voice);
    }
    @Override
    public void run() {
        System.out.println("Слон бежит");
    }

    @Override
    public String voice() {
        return voice;
    }
}