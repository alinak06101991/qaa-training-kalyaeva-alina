package model;

public class Kotik {

    private int satiety;
    public String catName;
    private String foodName;
    private String catVoice;
    public static int catCount;

    //Конструктор для объекта с аргументами
    public Kotik(String catVoice, String catName, int satiety) {
        catCount++;
        this.catVoice = catVoice;
        this.catName = catName;
        this.satiety = satiety;
    }

    //Конструктор для объекта без аргументов
    public Kotik() {
        catCount++;
    }

    //Геттеры и сеттеры переменных
    //Сытость
    public int getSatiety() {
        return satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    //Имя
    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    //Голос
    public String getCatVoice() {
        return catVoice;
    }

    public void setCatVoice(String catVoice) {
        this.catVoice = catVoice;
    }

    //Еда
    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    //Количество котов
    public int getCatCount() {
        return catCount;
    }

    public void setCatCount(int catCount) {
        this.catCount = catCount;
    }

    //Создание методов
    public void sleep() {
        if (satiety <= 0) {
            System.out.println("Покорми меня,у меня сытость " + satiety);
            eat();
        } else {
            System.out.println("Cat sleep ");
            satiety--;
            System.out.println("После метода sleep сытость стала " + satiety);
        }
    }

    public void play() {
        if (satiety <= 0) {
            System.out.println("Покорми меня,у меня сытость " + satiety);
            eat();
        } else {
            System.out.println("Cat play ");
            satiety--;
            System.out.println("После метода play сытость стала " + satiety);
        }
    }

    public void chaseMouse() {
        if (satiety <= 0) {
            System.out.println("Покорми меня,у меня сытость " + satiety);
            eat();
        } else {
            System.out.println("chaseMouse");
            satiety--;
            System.out.println("После метода chaseMouse сытость стала " + satiety);
        }
    }

    public void walk() {
        if (satiety <= 0) {
            System.out.println("Покорми меня,у меня сытость " + satiety);
            eat();
        } else {
            System.out.println("walk");
            satiety--;
            System.out.println("После метода walk сытость стала " + satiety);
        }
    }

    public void eat(String foodName, int satiety) {
        this.satiety = satiety;
        this.foodName = foodName;
        System.out.println("Eat");
    }

    public void eat(int satiety) {
        this.satiety = satiety;
        System.out.println("Eat");
    }

    public void eat() {
        eat("milk", 3);
        //System.out.println("Уровень сытости повысился на " + satiety);
    }

    public void drink() {
        if (satiety <= 0) {
            System.out.println("Покорми меня,у меня сытость " + satiety);
            eat();
        } else
            System.out.println("Drink water");
        satiety--;
        System.out.println("После метода drink сытость стала " + satiety);
    }

    public void liveAnotherDay() {
        for (int hour = 0; hour < 24; hour++) {
            int randomNumber = (int) (Math.random() * 5 + 1);
            //System.out.println((int)(Math.random()*5 +1));
            switch (randomNumber) {
                case 1:
                    walk();
                    break;
                case 2:
                    drink();
                    break;
                case 3:
                    play();
                    break;
                case 4:
                    sleep();
                    break;
                case 5:
                    chaseMouse();
                    break;
            }
        }
    }
}
