import animals.Animals;
import enums.AviarySize;

import java.util.HashMap;
import java.util.Map;

public class Generic<T extends Animals> {

    private AviarySize aviarySize;

    //Конструктор для вольера, в котором задается размер вольера
    public Generic(AviarySize aviarySize) {
        this.aviarySize = aviarySize;
    }

    private Map<String, Animals> map = new HashMap<String, Animals>();

    //Метод добавляет животное в вольер
    public void addAnimal(String name, Animals animal) {
        if (this.aviarySize != animal.aviarySize) {
            System.out.println("Не подходит размер вольера для " + animal.getName());
            return;
        }
        map.put(name, animal);
    }

    // Метод удаляет животное по идентификатору - имя
    public void removeAnimal(String name) {
        map.remove(name);
    }

    //Метод выводит ссылку на животное в вольере
    public T getAnimal(String name) {
        return (T) map.get(name);
    }


}
