package animals;

import enums.AviarySize;
import exceptions.WrongFoodException;
import food.Food;
import food.Grass;

public class Herbivore extends Animals {

    public Herbivore(String name, int satiety, String voice, AviarySize aviarySize) {
        super(name, satiety, voice, aviarySize);
    }

    public Herbivore(String name, int satiety, AviarySize aviarySize) {
        super(name, satiety,aviarySize);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Grass) {
            satiety += food.getSatiety();
            System.out.println("Травоядное животное поело траву");
        } else {
            System.out.println("Травоядное не ест мясо");
            throw new WrongFoodException("Выведено исключение:Травоядное не ест мясо");
        }
    }
}