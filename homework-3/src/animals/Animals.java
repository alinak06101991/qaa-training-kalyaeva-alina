package animals;

import enums.AviarySize;
import exceptions.WrongFoodException;
import food.Food;

import java.util.Objects;

public abstract class Animals {

    public String getName() {
        return name;
    }

    public int getSatiety() {
        return satiety;
    }

    public String getVoice() {
        return voice;
    }

    protected String name;
    protected int satiety;
    protected String voice;
    public AviarySize aviarySize;

    //Создать конструктор для всех животных
    public Animals(String name, int satiety, String voice, AviarySize aviarySize) {
        this.satiety = satiety;
        this.name = name;
        this.voice = voice;
        this.aviarySize = aviarySize;
    }

    public Animals(String name, int satiety, AviarySize aviarySize) {
        this.satiety = satiety;
        this.name = name;
        this.aviarySize = aviarySize;
    }

    public abstract void eat(Food food) throws WrongFoodException;

    public String voice () {
        return voice;
    }

    @Override
    public boolean equals(Object animals) {
        return name.equals(((Animals) animals).name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, voice, satiety);
    }
}



