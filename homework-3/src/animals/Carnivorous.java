package animals;

import enums.AviarySize;
import exceptions.WrongFoodException;
import food.Food;
import food.Meat;

public class Carnivorous extends Animals {

    public Carnivorous(String name, int satiety, String voice, AviarySize aviarySize) {
        super(name, satiety, voice, aviarySize);
    }

    public Carnivorous(String name, int satiety, AviarySize aviarySize) {
        super(name, satiety, aviarySize);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Meat) {
            satiety += food.getSatiety();
            System.out.println("Хищное животное поело мясо");
        } else {
            System.out.println("Хищник не ест траву");
            throw new WrongFoodException("Выведено исключение:Хищник не ест траву");
        }
    }
}
