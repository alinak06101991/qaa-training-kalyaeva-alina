package units;

import animals.Carnivorous;
import animals.Swim;
import enums.AviarySize;

public class Fish extends Carnivorous implements Swim {

    public Fish(String name, int satiety, AviarySize aviarySize) {
        super(name, satiety,aviarySize);
    }

    @Override
    public void swim() {
        System.out.println("Рыба плывет");
    }
}

