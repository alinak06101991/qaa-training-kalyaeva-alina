package units;

import animals.Fly;
import animals.Herbivore;
import animals.Swim;
import animals.Voice;
import enums.AviarySize;

public class Duck extends Herbivore implements Fly, Voice, Swim {

    public Duck(String name, int satiety, String voice, AviarySize aviarySize) {
        super(name, satiety, voice, aviarySize);
    }

    @Override
    public void fly() {
        System.out.println("Утка летит");
    }

    @Override
    public void swim() {
        System.out.println("Утка плывет");
    }

    @Override
    public String voice() {
        return voice;
    }

}