package units;

import animals.Herbivore;
import animals.Run;
import animals.Voice;
import enums.AviarySize;

public class Rhino extends Herbivore implements Run, Voice {

    public Rhino(String name, int satiety, AviarySize aviarySize) {
        super(name, satiety,aviarySize);
    }
    @Override
    public void run() {
        System.out.println("Носорог бежит");
    }

    @Override
    public String voice() {
        return voice;
    }
}
