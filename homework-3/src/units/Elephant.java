package units;

import animals.Herbivore;
import animals.Run;
import animals.Voice;
import enums.AviarySize;

public class Elephant extends Herbivore implements Run, Voice {

    public Elephant(String name, int satiety, String voice, AviarySize aviarySize) {
        super(name, satiety, voice,aviarySize);
    }
    @Override
    public void run() {
        System.out.println("Слон бежит");
    }

    @Override
    public String voice() {
        return voice;
    }
}