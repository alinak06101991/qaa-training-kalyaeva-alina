package units;

import animals.Carnivorous;
import animals.Run;
import animals.Voice;
import enums.AviarySize;
import food.Food;
import food.Meat;

public class Lion extends Carnivorous implements Run, Voice {

    public Lion(String name, int satiety, String voice, AviarySize aviarySize) {
        super(name, satiety, voice,aviarySize);
    }
    @Override
    public void run() {
        System.out.println("Lion бежит");
    }

    @Override
    public String voice() {
        return voice;
    }
    }
