package units;

import animals.Carnivorous;
import animals.Run;
import animals.Voice;
import enums.AviarySize;

public class Cheetos extends Carnivorous implements Run, Voice {

    public Cheetos(String name, int satiety, String voice, AviarySize aviarySize) {
        super(name, satiety, voice,aviarySize);
    }

    @Override
    public void run() {
        System.out.println("Гепард бежит");
    }

    @Override
    public String voice() {
        return voice;
    }

}
