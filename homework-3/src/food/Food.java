package food;

public class Food {

    public int satiety;
    public  String foodName;

    public int getSatiety() {
        return satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    //Создать конструктор для всей еды
    public Food(String foodName, int satiety ) {
        this.satiety = satiety;
        this.foodName = foodName;
    }
}
