package enums;

public enum AviarySize {
    SMALL,
    MEDIUM,
    BIG,
    XXL
}
