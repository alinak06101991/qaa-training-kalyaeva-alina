import exceptions.WrongFoodException;
import units.*;
import animals.*;
import food.*;

import java.util.HashMap;
import java.util.Map;
import enums.AviarySize;

import static enums.AviarySize.*;


/**
 * Основной класс прграммы
 */
public class Zoo {

    public static void main(String[] args) {

        //Создание вольера с типом Травоядные и размером вольера
        /**
         *
         */
        Generic <Herbivore> herbivoreGeneric= new Generic<Herbivore>(AviarySize.SMALL);
        herbivoreGeneric.addAnimal("Дональд", new Duck("Дональд", 5, "кря-кря", SMALL));
        Generic <Herbivore> herbivoreGeneric2= new Generic<Herbivore>(AviarySize.XXL);
        herbivoreGeneric2.addAnimal("Питер", new Elephant("Питер", 8, "ууууу", SMALL));
        Generic <Herbivore> herbivoreGeneric3= new Generic<Herbivore>(AviarySize.BIG);
        herbivoreGeneric3.addAnimal("Клаус", new Rhino("Клаус", 2,BIG));

        //Создание вольера с типом Хищники и размером вольера
        Generic <Carnivorous> carnivorousGeneric= new Generic<Carnivorous>(AviarySize.MEDIUM);
        carnivorousGeneric.addAnimal("Симба", new Lion("Симба", 6, "РРРРР",MEDIUM));
        Generic <Carnivorous> carnivorousGeneric2= new Generic<Carnivorous>(AviarySize.BIG);
        carnivorousGeneric2.addAnimal("Немо", new Fish("Немо", 1, BIG));
        Generic <Carnivorous> carnivorousGeneric3= new Generic<Carnivorous>(AviarySize.SMALL);
        carnivorousGeneric3.addAnimal("Лео", new Cheetos("Лео", 3, "ррррррр",BIG));


        Duck duck = new Duck("Дональд", 5, "кря-кря",MEDIUM);
        Fish fish = new Fish("Немо", 1,SMALL);
        Cheetos cheetos = new Cheetos("Лео", 3, "ррррррр",SMALL);
        Elephant elephant = new Elephant("Питер", 8, "ууууу",XXL);
        Lion lion = new Lion("Симба", 6, "РРРРР",BIG);
        Rhino rhino = new Rhino("Клаус", 2,BIG);
        Worker worker = new Worker("Вася", 40);
        Grass grass = new Grass("трава", 1);
        Meat meat = new Meat("свинина", 2);
        try {
            System.out.println(duck.getSatiety());
            worker.feed(grass, duck);
            worker.getVoice(duck);
            System.out.println(lion.getSatiety());
            worker.feed(meat, lion);
            System.out.println(lion.getSatiety());
            worker.getVoice(lion);
            System.out.println(fish.getSatiety());
            worker.feed(grass, fish);
            System.out.println(fish.getSatiety());
            worker.feed(meat, elephant);
        }catch (WrongFoodException ex) {
           System.out.println(ex.getMessage());
        }

        //создать массив - пруд с плавающими животными
        Swim[] pool = {duck, fish};
        for (int i = 0; i < pool.length; i++) {
            pool[i].swim();
        }
    }
}