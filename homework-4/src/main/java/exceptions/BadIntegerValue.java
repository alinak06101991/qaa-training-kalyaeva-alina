package exceptions;

/**
 * Исключение в случае наличия некорректного символа
 */
public class BadIntegerValue extends RuntimeException {

    public BadIntegerValue (String message) {
        //Вызов конструктора родителя
        super(message);
    }
}
