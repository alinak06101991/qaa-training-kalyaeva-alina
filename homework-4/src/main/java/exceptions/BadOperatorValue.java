package exceptions;

/**
 * Исключение в случае наличия некорректного оператора
 */
public class BadOperatorValue extends RuntimeException {

    public BadOperatorValue (String message) {
        //Вызов конструктора родителя
        super(message);
    }
}