package validator;

import exceptions.BadIntegerValue;
import exceptions.BadOperatorValue;

/**
 * Класс для проверки на валидацию
 */
public class Validator {

    /**
     * Проверяет число на тип - целое число
     *
     * @param input проверяемая строка
     * @return число в случае успешной проверки,исключение в случае неуспешной.
     */
    public static Integer validateInputInteger (String input) throws BadIntegerValue {
        if (input.matches("[0-9]*") && !input.isEmpty()) {
            return Integer.parseInt(input);
        } else {
            throw new BadIntegerValue("String have a bad symbol - " + input);
        }
    }

    /**
     * Проверяет оператор на вхождение в допустимый набор символов
     * @param input проверяемая строка
     * @return оператор в случае успешной проверки, исключение в случае неуспешной
     * @throws BadOperatorValue исключение
     */
    public static String validateInputOperator (String input) throws BadOperatorValue {
        if (input.matches("[/+/*//-]") && !input.isEmpty()) {
            return input;
        } else {
            throw new BadOperatorValue("String have a bad symbol - " + input);
        }
    }
}
