import exceptions.BadIntegerValue;
import exceptions.BadOperatorValue;

import java.util.Scanner;
import model.Calculator;
import validator.Validator;

/**
 * Старт программы
 */

public class Main {

    public static void main(String[] args) {
        System.out.println("Запуск калькулятора");

        // Бескоченый цикл, чтобы выполнение программы не прекращалось
        while (true) {

            // Создание сканера, который считывает значение с консоли.
            System.out.println("Введите первое число");
            //Создание объекта сканер, чтобы получить ввод пользователя с консоли
            Scanner scanner = new Scanner(System.in);
// Ловим исключение
           try {
               // Проверка ввода на тип данных
               Integer a = Validator.validateInputInteger(scanner.next());

               System.out.println("Введите оператор (+, -, /, *)");
               //Объявление переменной содержащей оператор, которая валидируется методом, созданным в валидации
               String oper = Validator.validateInputOperator(scanner.next());

               System.out.println("Введите второе число");
               // Проверка ввода на тип данных
               Integer b = Validator.validateInputInteger(scanner.next());

               System.out.println(a+oper+b);
               //Создание объект калькулятор
               Calculator calc = new Calculator();

               //Запуск работы калькулятора
               System.out.println("Результат: "+calc.exec(a,oper,b));
               //Перечисление доступных исключений
           }catch (BadIntegerValue | BadOperatorValue | ArithmeticException  e  ){
               //Вывод сообщения из исключения
                System.out.println("Исключение в логике программы "+e.getMessage());
            }



        }

    }
}