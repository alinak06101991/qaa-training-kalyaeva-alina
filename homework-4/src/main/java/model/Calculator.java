package model;

/**
 * Создание класса калькулятора
 */
public class Calculator {

    //Создание метода, который производит вычисления
    public int exec(int a,String oper,int b){
        //Запуск условного оператора
        switch (oper){
            case "+":
                return a+b;
            case "-":
                return a-b;
            case "/":
                return a/b;
            case "*":
                return a*b;
        }
        //Если введен символ отличный от оператора, то выводится сообщение
        System.out.println("Оператор не поддерживается");

        //В случае, если оператор введен неверно, то вернется 0
        return 0;
    }
}
