package parametrized_test;


import io.qameta.allure.*;
import model.Calculator;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Набор тестов с DataProvider для класса Сalculator
 */
@Epic("Сalculator")
@Feature("Parameterized tests. Валидные вычисления")

public class СalculatorDataProvider {

    /**
     * Создание переменной с классом Сalculator
     */
    private Calculator calculator;

    /**
     * Метод который выполняется перед каждый тестом, создает объект класса.
     */
    @BeforeMethod
    public void setUp() {
        calculator = new Calculator();
    }

    /**
     * DataProvider с тестовыми данными и ожидаемым результатом для тестов
     *
     * @return двумерный массив объектов (стандартный return для DataProvider)
     */
    @DataProvider
    public Object[][] getTestAndExpectedData() {
        return new String[][]{
                {"1", "+", "3", "4"},
                {"5", "-", "2", "3"}

        };
    }

    /**
     * Позитивная проверка сложения
     */

    @Test(dataProvider = "getTestAndExpectedData")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Добавление строки в Concatenator с Data Provider")
    @Description("Тест выполнения валидных вычислений с Data Provider")

    public void testСalculPositive(String str1, String str2, String str3, String expected) {
        int result = calculator.exec(Integer.parseInt(str1),str2,Integer.parseInt(str3));
        assertEquals(result, expected);
    }
}
