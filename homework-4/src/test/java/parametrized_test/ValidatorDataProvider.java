package parametrized_test;

import exceptions.BadOperatorValue;
import io.qameta.allure.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static validator.Validator.validateInputOperator;

/**
 * Набор тестов с DataProvider для класса Validator
 */
@Epic("Validator")
@Feature("Parameterized tests. Валидация введенных значений")

public class ValidatorDataProvider {

    /**
     * DataProvider с тестовыми данными для позитивных тестов
     *
     * @return двумерный массив объектов (стандартный return для DataProvider)
     */
    @DataProvider
    public Object[][] getTestDataPositive() {
        return new String[][]{
                { "+", "+"},
                { "-", "-"},
                { "*", "*"},
                { "/","/"}
        };
    }

    /**
     * DataProvider с тестовыми данными для негативных тестов
     *
     * @return двумерный массив объектов (стандартный return для DataProvider)
     */
    @DataProvider
    public Object[][] getTestDataNegative() {
        return new String[][]{
                {"|%$#", "|%$#"},
                {" ", " "},
                {"", ""}
        };
    }

    /**
     * Параметризированный позитивный тест
     *
     * @param str тестовые данные из DataProvider
     */
    @Test(dataProvider = "getTestDataPositive")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Валидация операторов в Calculator с Data Provider")
    @Description("Тест позитивным набором проверок в Data Provider")

    public void testValidateInputStringPositive(String str, String expected) {
        String result = validateInputOperator(str);
        assertEquals(result, expected);
    }

    /**
     * Параметризированный негативный тест
     *
     * @param str тестовые данные из DataProvider
     */
    @Test(dataProvider = "getTestDataNegative", expectedExceptions = BadOperatorValue.class)
    @Severity(SeverityLevel.MINOR)
    @Story("Валидация операторов в Calculator с Data Provider")
    @Description("Тест с негативным набором проверок в Data Provider")

    public void testValidateInputStringNegative(String str, String expected) {
        validateInputOperator(str);
    }
}
