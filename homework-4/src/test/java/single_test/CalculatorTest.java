package single_test;

import io.qameta.allure.*;
import model.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Набор отдельных тестов для класса Calculator
 */
@Epic("Calculator")
@Feature("Single tests. Выполнение сложения")

public class CalculatorTest {
    /**
     * Создание переменной с классом Calculator.
     */
    private Calculator calculator;

    /**
     * Метод который выполняется перед каждый тестом, создает объект класса.
     */
    @BeforeMethod
    public void setUp() {
        calculator = new Calculator();
    }

    /**
     * Позитивная проверка сложения
     */
    @Test (description = "Позитивная проверка сложения двух чисел")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Вычисления")
    @Description("Тест проверяет, что сложение двух чисел в результате дает верное значение")

    public void testPutSumPositive() {
       int result = calculator.exec(2,"+",2);
        Assert.assertEquals(result, 4);
    }

    /**
     * Позитивная проверка вычитания
     */

    @Test (description = "Позитивная проверка вычитания двух чисел")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Вычисления")
    @Description("Тест проверяет, что вычитание двух чисел в результате дает верное значение")

    public void testPutSubPositive() {
        int result = calculator.exec(10,"-",5);
        Assert.assertEquals(result, 5);
    }

    /**
     * Позитивная проверка деления
     */
    @Test (description = "Позитивная проверка деления двух чисел")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Вычисления")
    @Description("Тест проверяет, что деление чисел одно на другое в результате дает верное значение")

    public void testPutDivPositive() {
        int result = calculator.exec(20,"/",2);
        Assert.assertEquals(result, 10);
    }

    /**
     * Позитивная проверка умножения
     */
    @Test (description = "Позитивная проверка умножения двух чисел")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Вычисления")
    @Description("Тест проверяет, что умножение чисел одно на другое в результате дает верное значение")

    public void testPutMulPositive() {
        int result = calculator.exec(3,"*",2);
        Assert.assertEquals(result, 6);
    }

}
