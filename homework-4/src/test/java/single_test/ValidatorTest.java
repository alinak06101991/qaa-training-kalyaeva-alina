package single_test;

import exceptions.BadIntegerValue;
import exceptions.BadOperatorValue;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;

import static validator.Validator.validateInputInteger;
import static validator.Validator.validateInputOperator;

/**
 * Набор отдельных тестов для класса Validator
 */
@Epic("Validator")
@Feature("Single tests. Валидация аргументов и операторов")

public class ValidatorTest {

    /**
     * Проверка валидации числа на тип (позитивная)
     */
    @Test (description = "Позитивная проверка на соответствие типу int")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Валидация на тип аргументов")
    @Description("Тест проверяет, что интовое значение принимается как валидный тип данных")

    public void testValidateInteger() {
        String number = "5";
        int result = validateInputInteger (number);
        Assert.assertEquals(result, 5);
    }

    /**
     * Проверка валидации числа на строку (вернется исключение)
     */
    @Test(expectedExceptions = BadIntegerValue.class, description = "Позитивная проверка на соответствие типу int")
    @Severity(SeverityLevel.MINOR)
    @Story("Валидация на тип аргументов")
    @Description("Тест проверяет, что строковое значение будет выводить эксепшен, что тип некорректный")

    public void testValidateIntegerString() {
        String number = "абв";
        validateInputInteger (number);
    }

    /**
     * Проверка валидации числа на пробел (вернется исключение)
     */
    @Test(expectedExceptions = BadIntegerValue.class, description = "Позитивная проверка на соответствие типу int")
    @Severity(SeverityLevel.NORMAL)
    @Story("Валидация на тип аргументов")
    @Description("Тест проверяет, что ввод пробела в аргументе будет выводить эксепшен, что тип некорректный")

    public void testValidateIntegerSpace() {
        String number = " ";
        validateInputInteger (number);
    }

    /**
     * Проверка валидации числа с пустой строкой (вернется исключение)
     */
    @Test(expectedExceptions = BadIntegerValue.class, description = "Позитивная проверка на соответствие типу int")
    @Severity(SeverityLevel.MINOR)
    @Story("Валидация на тип аргументов")
    @Description("Тест проверяет, что не ввод значения в аргумент будет выводить эксепшен, что тип некорректный")

    public void testValidateIntegerEmpty() {
        String number = "";
        validateInputInteger (number);
    }

    /**
     * Проверка валидации оператора на допустимый набор операторов (позитивная проверка)
     */
    @Test (description = "Позитивная проверка на соответствие доспустимому набору операторов")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Валидация на тип оператора")
    @Description("Тест проверяет, что ввод допустимого оператора не дает ошибки")

    public void testValidateOperator() {
        String str = "+";
        String result =  validateInputOperator(str);
        Assert.assertEquals(result, str);
    }

    /**
     * Проверка валидации оператора на ввод строки (вернется исключение)
     */
    @Test(expectedExceptions = BadOperatorValue.class, description = "Позитивная проверка что ввод значений типа строка в качестве оператора невозможен")
    @Severity(SeverityLevel.MINOR)
    @Story("Валидация на тип оператора")
    @Description("Тест проверяет, что ввод строкового значения в оператора дает ошибку")

    public void testValidateOperatorString() {
        String str = "as";
      validateInputOperator(str);
    }

    /**
     * Проверка валидации оператора на ввод числа (вернется исключение)
     */
    @Test(expectedExceptions = BadOperatorValue.class, description = "Позитивная проверка что ввод значений типа число в качестве оператора невозможен")
    @Severity(SeverityLevel.MINOR)
    @Story("Валидация на тип оператора")
    @Description("Тест проверяет, что ввод числа в оператора дает ошибку")

    public void testValidateOperatorNumber() {
        String str = "7";
        validateInputOperator(str);
    }

    /**
     * Проверка валидации оператора на пустую строку (вернется исключение)
     */
    @Test(expectedExceptions = BadOperatorValue.class, description = "Проверка что оставить оператор в виде пустой строки даст ошибку валидации")
    @Severity(SeverityLevel.MINOR)
    @Story("Валидация на тип оператора")
    @Description("Тест проверяет, что не ввод значения в оператора дает ошибку")


    public void testValidateOperatorEmpty() {
        String str = "";
        validateInputOperator(str);
    }

    /**
     * Проверка валидации оператора на пробел (вернется исключение)
     */
    @Test(expectedExceptions = BadOperatorValue.class, description = "Проверка что ввод в качестве оператора пробела даст ошибку валидации")
    @Severity(SeverityLevel.MINOR)
    @Story("Валидация на тип оператора")
    @Description("Тест проверяет, что ввод в качестве оператора пробела даст ошибку валидации")

    public void testValidateOperatorSpace() {
        String str = " ";
        validateInputOperator(str);
    }
}

